﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Runaurufu.Data.Connection;
using Runaurufu.Serialization;
using Runaurufu.Extension;
using System.IO;
using System.Reflection;
using Runaurufu.Reflection;

namespace LokiORM
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    Converter converter = new Converter();
    public MainWindow()
    {
      InitializeComponent();

      AppDomainAssemblyCollector.BindApplicationDomain();
      AppDomainAssemblyCollector.AddAssemblyDirectory(System.IO.Path.GetDirectoryName(System.Windows.Forms.Application.ExecutablePath));
      AppDomainAssemblyCollector.LoadAllAssemblies(false);

      TypesManager.AssemblyCollector = new AppDomainAssemblyCollector();

      if (this.LoadXmlConfigs())
      {
        foreach (LokiModelTemplate temp in LokiModelTemplate.List)
        {
          OrmExportTemplateComboBox.Items.Add(temp);
        }
        OrmExportTemplateComboBox.SelectedIndex = 0;
      }

      ImportDatabaseEngineComboBox.Items.Clear();
      
      // must be after dictionary load
      TypesManager.Rebuild();
      Type[] connectionTypes = TypesManager.GetTypesImplementingInterface(typeof(IDatabaseConnectorNativeConnection));

      foreach (Type t in connectionTypes)
      {
        if (t.IsClass && t.IsAbstract == false)
          ImportDatabaseEngineComboBox.Items.Add(t);
      }
      

      // ImportDatabaseEngineComboBox.Items.Add(ConnectorType.MySql);
      // ImportDatabaseEngineComboBox.Items.Add(ConnectorType.Oracle);
      ImportDatabaseEngineComboBox.SelectedIndex = 0;
    }

    private FileInfo[] ListXmlFiles(String folderPath)
    {
      DirectoryInfo di = new DirectoryInfo(folderPath);
      return di.GetFiles("*.xml");
    }

    private Boolean LoadXmlConfigs()
    {
      FileInfo[] files = ListXmlFiles("dictionaries");
      foreach (FileInfo fi in files)
      {
        LokiDictionary lDict = new LokiDictionary();
        if (lDict.AcquireFromXml(fi.FullName))
        {
          LokiDictionary.List.Add(lDict);
        }
      }

      files = ListXmlFiles("templates");
      foreach (FileInfo fi in files)
      {
        LokiModelTemplate lTemp = new LokiModelTemplate();
        if (lTemp.AcquireFromXml(fi.FullName))
        {
          if (lTemp.FindDictionary())
          {
            LokiModelTemplate.List.Add(lTemp);
          }
        }
      }

      if (LokiDictionary.List.Count == 0)
      {
        MessageBox.Show("No dictionary files loaded - please reinstall application");
        return false;
      }
      if (LokiModelTemplate.List.Count == 0)
      {
        MessageBox.Show("No template files loaded - please reinstall application");
        return false;
      }
      return true;
    }

    private void ImportConvertButton_Click(object sender, RoutedEventArgs e)
    {
      converter.EnabledImportConversions.Clear();
      converter.EnabledImportConversions.AddRange(ImportConvertListBox.SelectedItems.Cast<ConvertOption>());
      converter.PerformImportConversion();
    }

    private void ImportDatabaseEngineComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      Type t = ImportDatabaseEngineComboBox.SelectedItem as Type;
      if (t == null)
        return;
      converter.Connector = (IDatabaseConnectorNativeConnection)Activator.CreateInstance(t, ImportConnectionStringTextBox.Text);
      converter.GetProperDatabaseImportDictionary(LokiDictionary.List);
      this.ImportConvertListBoxRefresh(converter.ImportDictionary);
    }

    private void ImportConvertListBoxRefresh(LokiDictionary dict)
    {
      ImportConvertListBox.Items.Clear();
      foreach (ConvertOption opt in dict.ConvertOptions)
      {
        ImportConvertListBox.Items.Add(opt);
      }
    }

    private void ImportClearSelectionButton_Click(object sender, RoutedEventArgs e)
    {
      ImportConvertListBox.UnselectAll();
    }

    private void ExportClearSelectionButton_Click(object sender, RoutedEventArgs e)
    {
      ExportConvertListBox.UnselectAll();
    }

    private void ExportConvertButton_Click(object sender, RoutedEventArgs e)
    {
      converter.EnabledExportConversions.Clear();
      converter.EnabledExportConversions.AddRange(ExportConvertListBox.SelectedItems.Cast<ConvertOption>());
      converter.PerformExportConversion();
    }

    private void OrmExportTemplateComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      LokiModelTemplate temp = (LokiModelTemplate)OrmExportTemplateComboBox.SelectedItem;
      converter.ExportDictionary = temp.Dictionary;
      converter.Template = temp;
      this.ExportConvertListBoxRefresh(converter.ExportDictionary);
      this.OrmOptionsListBoxRefresh();

    }

    private void OrmOptionsListBoxRefresh()
    {
      OrmOptionsDataGrid.ItemsSource = converter.Template.Fields;
    }

    private void ExportConvertListBoxRefresh(LokiDictionary dict)
    {
      ExportConvertListBox.Items.Clear();
      foreach (ConvertOption opt in dict.ConvertOptions)
      {
        ExportConvertListBox.Items.Add(opt);
      }
    }

    private void ImportTestConnectionButton_Click(object sender, RoutedEventArgs e)
    {

      try
      {
        converter.Connector.ConnectionString = ImportConnectionStringTextBox.Text;
        converter.Connector.Open();
        converter.Connector.Close();
      }
      catch (Exception ex)
      {
        System.Windows.MessageBox.Show("Unable to connect to database\n" + ex.Message);
        return;
      }
      System.Windows.MessageBox.Show("Database available");
    }

    private void ImportAcquireStructureButton_Click(object sender, RoutedEventArgs e)
    {
      converter.Connector.ConnectionString = ImportConnectionStringTextBox.Text;
      try
      {
        converter.Connector.Open();
      }
      catch (Exception ex)
      {
        System.Windows.MessageBox.Show("Unable to connect to database\n" + ex.Message);
        return;
      }
      converter.GetDatabaseStructure();
      this.ImportNumberOfTablesTextBox.Text = converter.Classes.Count.ToString();
      converter.Connector.Close();
    }

    private void ORMOptionsDataGridValueTextBox_LostFocus(object sender, RoutedEventArgs e)
    {
      TextBox tb = (TextBox)sender;
      String name = tb.Tag.ToString();
      foreach (Field item in converter.Template.Fields)
      {
        if (item.Name == name)
        {
          item.Value = tb.Text;
        }
      }
    }

    private void OrmBrowseSavePath_Click(object sender, RoutedEventArgs e)
    {
      System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
      fbd.SelectedPath = OrmModelSavePathTextBox.Text;

      if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
      {
        OrmModelSavePathTextBox.Text = fbd.SelectedPath;
        converter.ExportPath = fbd.SelectedPath;
      }
    }

    private void OrmGenerateModelsButton_Click(object sender, RoutedEventArgs e)
    {
      foreach (ClassInfo ci in converter.Classes)
      {
        this.createClassModel(converter.ExportPath, ci, converter.Template);
      }
      MessageBox.Show("Models generated");
    }

    private Boolean createClassModel(String folderPath, ClassInfo ci, LokiModelTemplate template)
    {
      string[] schemesOrder = template.SchemesOrder;


      Dictionary<String, String> options = new Dictionary<String, String>();
      options.Add("parentClassName", template.Schemes["DefaultParentClassName"]);
      options.Add("access", "private");

      String className = ci.Name;
      if (template.Schemes.ContainsKey("FileClassName"))
        className = template.Parse("FileClassName", ci, options);

      className = Char.ToUpper(className[0]) + className.Substring(1).Replace(":", "");

      string fileName = template.FileName;
      fileName = template.ParseString(fileName, ci, options);

      using (FileStream stream = new FileStream(folderPath + System.IO.Path.DirectorySeparatorChar + fileName, FileMode.Create, FileAccess.Write, FileShare.None))
      {
        using (StreamWriter writer = new StreamWriter(stream))
        {
          foreach (string schemaName in schemesOrder)
          {
            if (template.Schemes.ContainsKey(schemaName))
              writer.WriteLine(template.Parse(schemaName, ci, options));
            else if (string.Equals(schemaName, "#VariableInitialization#", StringComparison.InvariantCultureIgnoreCase))
            {
              foreach (VariableInfo vi in ci.Vars.Values)
              {
                if ((vi.DefaultValue == null || vi.DefaultValue is DBNull) && vi.StrictStructure.IsNullable == false)
                {
                  writer.WriteLine(template.Parse("Variable", vi, options));
                }
                else
                {
                  writer.WriteLine(template.Parse("InitializatedVariable", vi, options));
                }
              }
            }
            else if (string.Equals(schemaName, "#VariableAccess#", StringComparison.InvariantCultureIgnoreCase))
            {
              foreach (VariableInfo vi in ci.Vars.Values)
              {
                writer.WriteLine(template.Parse("VariableAccess", vi, options));
              }
            }
          }
        }
      }
      return true;
    }

    private void OrmModelSavePathTextBox_LostFocus(object sender, RoutedEventArgs e)
    {
      converter.ExportPath = OrmModelSavePathTextBox.Text;
    }
  }
}
