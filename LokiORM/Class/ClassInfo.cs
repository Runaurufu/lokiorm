﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Runaurufu.Data.Connection;

namespace LokiORM
{
  class ClassInfo
  {
    private string name;
    public string Name
    {
      get { return this.name; }
      set { this.name = value; }
    }

    private ClassInfo parentClass = null;
    public ClassInfo ParentClass
    {
      get { return this.parentClass; }
      set { this.parentClass = value; }
    }

    private static Dictionary<String, ClassInfo> list = new Dictionary<String, ClassInfo>();
    public static Dictionary<String, ClassInfo> List
    { get { return ClassInfo.list; } }

    private Dictionary<String, VariableInfo> vars;
    public Dictionary<String, VariableInfo> Vars
    { get { return this.vars; } }

    private List<VariableInfo> identifyingVars;
    public List<VariableInfo> IdentifyingVars
    { get { return this.identifyingVars; } }

    public ClassInfo(TableStructure table)
    {
      this.name = table.TableName;
      this.vars = new Dictionary<String, VariableInfo>();
      this.identifyingVars = new List<VariableInfo>();

      foreach (ColumnStructure row in table.Columns)
      {
        VariableInfo var = new VariableInfo(row, this);
        this.vars.Add(var.Name, var);
        if (var.Identifying) { this.identifyingVars.Add(var); }
      }
    }



    public Boolean GenerateGenericType(LokiDictionary dictionary, List<ConvertOption> convertOptions)
    {
      foreach (VariableInfo var in this.vars.Values)
      {
        if (!var.GenerateGenericType(dictionary, convertOptions))
        {
          //             return false;
        }
      }
      return true;
    }

    //TODO
    public Boolean GenerateStrictType(LokiDictionary dictionary, List<ConvertOption> convertOptions)
    {
      foreach (VariableInfo var in this.vars.Values)
      {
        if (!var.GenerateStrictType(dictionary, convertOptions))
        {
          //             return false;
        }
      }
      return true;
    }


    public void GenerateRelatedVars(TableStructure table)
    {
      foreach (ColumnStructure row in table.Columns)
      {
        if (row.ForeignKey != null)
        {
          this.vars[row.Name].AssignRelated(row.ForeignKey.TableStructure.TableName, row.ForeignKey.Name);
        }
      }
    }
  }
}
