﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Runaurufu.Data.Connection;

namespace LokiORM
{
  class VariableInfo
  {
    private ClassInfo parentClass;
    private string orginalName;
    private string name;

    private StrictDataStructure strictStructure;
    private NativeDataStructure genericStructure;
    private String dataSize;

    private bool nullable;
    private bool identifying;
    private VariableInfo related = null;
    private object defaultValue;

    public StrictDataStructure StrictStructure
    {
      set { this.strictStructure = value; }
      get { return this.strictStructure; }
    }

    public NativeDataStructure GenericStructure
    {
      set { this.genericStructure = value; }
      get { return this.genericStructure; }
    }

    public String DataSize
    {
      get { return this.dataSize; }
    }

    public ClassInfo ParentClass
    {
      get { return this.parentClass; }
    }

    public String Name
    {
      get { return this.name; }
      set { this.name = value; }
    }

    public String OrginalName
    {
      get { return this.orginalName; }
      set { this.orginalName = value; }
    }

    public Boolean Identifying
    {
      get { return this.identifying; }
      set { this.identifying = value; }
    }

    public VariableInfo Related
    {
      set { this.related = value; }
      get { return this.related; }
    }

    public object DefaultValue
    {
      get { return this.defaultValue; }
      set { this.defaultValue = value; }
    }

    public VariableInfo(ColumnStructure row, ClassInfo parent)
    {
      this.parentClass = parent;
      this.orginalName = row.Name;
      this.name = this.orginalName.Replace(" ", "");

      this.strictStructure = row.StrictDataStructure;
      this.genericStructure = row.NativeDataStructure;

      this.identifying = row.IsPrimaryKey;
      this.defaultValue = row.DefaultValue;
    }

    public Boolean GenerateGenericType(LokiDictionary dictionary, List<ConvertOption> convertOptions)
    {
      foreach (ConvertOption co in convertOptions)
      {
        if (this.strictStructure.DataType == co.StrictType && this.strictStructure.Sizes.Length == co.StrictSize.Length)
        {
          Boolean flag = true;
          for (int i = 0; i < this.strictStructure.Sizes.Length; i++)
          {
            if (this.strictStructure.Sizes[i] > co.StrictSize[i] && co.StrictSize[i] >= 0)
            {
              flag = false;
              break;
            }
          }
          if (flag)
          {
            this.genericStructure.DataType = co.GenericType;
            this.genericStructure.Sizes = co.GenericSize;
            if (this.strictStructure.Sizes.Length == this.genericStructure.Sizes.Length)
            {
              for (int i = 0; i < this.genericStructure.Sizes.Length; i++)
              {
                if (this.genericStructure.Sizes[i] == -1)
                {
                  this.genericStructure.Sizes[i] = this.strictStructure.Sizes[i];
                }
              }
            }


            //TODO
            this.dataSize = genericStructure.Sizes.Length > 0 ? genericStructure.Sizes[0].ToString() : "";
            return true;
          }
        }
      }

      foreach (DictionaryEntry de in dictionary.Entries)
      {
        if (de.StrictType == this.strictStructure.DataType)
        {
          if (de.StrictSize.Length == 0) // catch all sizes
          {

          }
          else if (de.StrictSize.Length == this.strictStructure.Sizes.Length) // catch if there are sizes to compare
          {
            for (int i = 0; i < de.StrictSize.Length; i++)
            {
              if(this.strictStructure.Sizes[i] > de.StrictSize[i])
                goto NextDictionaryEntry; // if value is greater than value in dictionary than we check next dictionary entry
            }
          }
          else
          { // NEXT!
            goto NextDictionaryEntry;
          }

          // at that point we should have proper dictionary entry selected
          if (de.StrictTypeIsNullable.HasValue == false ||
            (de.StrictTypeIsNullable.HasValue == true && de.StrictTypeIsNullable.Value == this.strictStructure.IsNullable))
          {
            this.genericStructure.DataType = de.GenericType;
            if (de.StrictSize.Length == 0)
              this.genericStructure.Sizes = new Int64[0];
            else
            {
              this.genericStructure.Sizes = new Int64[de.GenericSize.Length];
              for (int i = 0; i < de.GenericSize.Length; i++)
              {
                if (de.GenericSize[i] == -1)
                  this.genericStructure.Sizes[i] = this.strictStructure.Sizes[i];
                else
                  this.genericStructure.Sizes[i] = de.GenericSize[i];
              }
            }
            this.dataSize = genericStructure.Sizes.Length > 0 ? genericStructure.Sizes[0].ToString() : "";
            return true;
          }
        }
      NextDictionaryEntry: ;
      }
      return false;
    }

    public Boolean GenerateStrictType(LokiDictionary dictionary, List<ConvertOption> convertOptions)
    {
      foreach (ConvertOption co in convertOptions)
      {
        if (this.genericStructure.DataType == co.GenericType && this.genericStructure.Sizes.Length == co.GenericSize.Length)
        {
          Boolean flag = true;
          for (int i = 0; i < this.genericStructure.Sizes.Length; i++)
          {
            if (this.genericStructure.Sizes[i] > co.GenericSize[i] && co.GenericSize[i] >= 0)
            {
              flag = false;
              break;
            }
          }
          if (flag)
          {
            this.strictStructure.DataType = co.StrictType;
            this.strictStructure.Sizes = co.StrictSize;
            if (this.strictStructure.Sizes.Length == this.genericStructure.Sizes.Length)
            {
              for (int i = 0; i < this.genericStructure.Sizes.Length; i++)
              {
                if (this.strictStructure.Sizes[i] == -1)
                {
                  this.strictStructure.Sizes[i] = this.genericStructure.Sizes[i];
                }
              }
            }
            return true;
          }
        }
      }

      foreach (DictionaryEntry de in dictionary.Entries)
      {
        if (de.GenericType == this.genericStructure.DataType)
        {
          this.strictStructure.DataType = de.StrictType;

          if (de.StrictSize.Length == 0)
          {
            this.strictStructure.Sizes = new Int64[0];
          }
          else
          {
            this.strictStructure.Sizes = new Int64[de.StrictSize.Length];
            if (this.strictStructure.Sizes.Length == this.genericStructure.Sizes.Length)
            {
              for (int i = 0; i < de.StrictSize.Length; i++)
              {
                if (de.StrictSize[i] == -1)
                {
                  this.strictStructure.Sizes[i] = this.genericStructure.Sizes[i];
                }
                else if (this.genericStructure.Sizes[i] <= de.StrictSize[i])
                {
                  this.strictStructure.Sizes[i] = de.StrictSize[i];
                }
                else
                {
                  goto NextDictionaryEntry;
                }
              }
            }
            else
            {
              this.strictStructure.Sizes = de.StrictSize;
            }
          }

          return true;
        }
      NextDictionaryEntry: ;
      }
      return false;
    }

    public void AssignRelated(String className, String variableName)
    {
      ClassInfo ci = ClassInfo.List[className];
      this.related = ci.Vars[variableName];
    }
  }
}
