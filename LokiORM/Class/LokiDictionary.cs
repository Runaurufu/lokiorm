﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Runaurufu.Data.Connection;

namespace LokiORM
{
  class DictionaryEntry
  {
    private String genericType;
    private Int64[] genericSize;
    private Int64[] strictSize;
    private StrictDataType strictType;
    private Boolean? strictTypeIsNullable;

    public Boolean? StrictTypeIsNullable
    {
      get { return this.strictTypeIsNullable; }
      set { this.strictTypeIsNullable = value; }
    }

    public String GenericType
    {
      get
      {
        return this.genericType;
      }
      set
      {
        this.genericType = value;
      }
    }

    public Int64[] GenericSize
    {
      get
      {
        return this.genericSize;
      }
      set { this.genericSize = value; }
    }

    public Int64[] StrictSize
    {
      get
      {
        return this.strictSize;
      }
      set { this.strictSize = value; }
    }

    public StrictDataType StrictType
    {
      get
      {
        return this.strictType;
      }
      set
      {
        this.strictType = value;
      }
    }
  }

  class DictionaryOption
  {
    private String type;
    private Dictionary<String, String> args = new Dictionary<string, string>();

    public String Type
    {
      get { return this.type; }
      set { this.type = value; }
    }

    public Dictionary<String, String> Args
    {
      get { return this.args; }
    }
    public override string ToString()
    {
      switch (type)
      {
        case "Convert":
          return type + ": " + this.args["SourceType"] + "(" + this.args["SourceSize"] + ") <=> " + this.args["OutputType"] + "(" + this.args["OutputSize"] + ")";
        default:
          return base.ToString();
      }
    }
  }

  public class ConvertOption
  {
    private String genericType;
    private Int64[] genericSize;
    private StrictDataType strictType;
    private Int64[] strictSize;

    public ConvertOption() { }

    public String GenericType
    { get { return this.genericType; } }
    public Int64[] GenericSize
    { get { return this.genericSize; } }
    public StrictDataType StrictType
    { get { return this.strictType; } }
    public Int64[] StrictSize
    { get { return this.strictSize; } }

    public ConvertOption(String genericType, Int64[] genericSize, StrictDataType strictType, Int64[] strictSize)
    {
      this.genericType = genericType;
      this.genericSize = genericSize;
      this.strictType = strictType;
      this.strictSize = strictSize;
    }

    public ConvertOption(String genericType, Int64[] genericSize, String strictType, Int64[] strictSize)
    {
      this.genericType = genericType;
      this.genericSize = genericSize;
      this.strictSize = strictSize;
      switch (strictType.ToLower())
      {
        case "binary": this.strictType = StrictDataType.Binary; break;
        case "bool": this.strictType = StrictDataType.Bool; break;
        case "char": this.strictType = StrictDataType.Char; break;
        case "datetime": this.strictType = StrictDataType.DateTime; break;
        case "time": this.strictType = StrictDataType.Time; break;
        case "decimal": this.strictType = StrictDataType.Decimal; break;
        case "float": this.strictType = StrictDataType.Float; break;
        case "int": this.strictType = StrictDataType.Int; break;
        case "unsigneddecimal": this.strictType = StrictDataType.UnsignedDecimal; break;
        case "unsignedfloat": this.strictType = StrictDataType.UnsignedFloat; break;
        case "unsignedint": this.strictType = StrictDataType.UnsignedInt; break;
        case "varchar": this.strictType = StrictDataType.VarChar; break;
        default: throw new Exception("Unsupported strictDateType in Convert Option: " + strictType);
      }
    }

    public override string ToString()
    {
      String ret = this.genericType + "(";
      foreach (Int64 item in this.genericSize)
      {
        ret += item + ", ";
      }
      ret += ") <=> " + this.strictType + "(";
      foreach (Int64 item in this.strictSize)
      {
        ret += item + ", ";
      }
      ret += ")";
      ret = ret.Replace(", )", ")");
      return ret;
    }

    /*
    public static List<ConvertOption> TakeSelectedOptions(System.Windows.Forms.CheckedListBox list)
    {
        List<ConvertOption> options = new List<ConvertOption>();
        foreach (var item in list.SelectedItems)
        {
            options.Add((ConvertOption)item);
        }
        return options;
    }
     * */
  }

  class LokiDictionary
  {
    //private UnifiedXml handle;
    private String name;
    private String dataPattern;
    private List<DictionaryEntry> entries = new List<DictionaryEntry>();
    private List<DictionaryOption> options = new List<DictionaryOption>();
    private List<ConvertOption> convertOptions = new List<ConvertOption>();
    private static List<LokiDictionary> list = new List<LokiDictionary>();

    public String Name
    {
      get { return this.name; }
    }

    public String DataPattern
    {
      get { return this.dataPattern; }
    }

    public List<DictionaryEntry> Entries
    {
      get { return this.entries; }
    }

    public List<DictionaryOption> Options
    {
      get { return this.options; }
    }

    public List<ConvertOption> ConvertOptions
    {
      get { return this.convertOptions; }
    }

    public static List<LokiDictionary> List
    {
      get
      {
        return list;
      }
    }

    public LokiDictionary()
    {
    }

    public LokiDictionary(String filepath)
    {
      if (!this.AcquireFromXml(filepath))
      {
        throw new Exception("Cannot acquire data from file");
      }
    }

    public static LokiDictionary GetFirstWithDataPattern(String pattern)
    {
      foreach (LokiDictionary item in LokiDictionary.list)
      {
        if (item.DataPattern == pattern)
        {
          return item;
        }
      }
      return null;
    }

    public Boolean AcquireFromXml(String filepath)
    {
      //this.handle = new UnifiedXml(filepath);
      //if (!this.handle.Read())
      //  return false;

      try
      {
        //XmlDocument xmlDoc = handle.XmlDoc;

        XmlDocument xmlDoc = new XmlDocument();
        using (FileStream reader = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
          xmlDoc.Load(reader);
        }

        this.name = xmlDoc.GetElementsByTagName("Name")[0].InnerText;
        this.dataPattern = xmlDoc.GetElementsByTagName("DataPattern")[0].InnerText;

        XmlNodeList xmlEntries = xmlDoc.GetElementsByTagName("Entry");

        entries.Clear();

        foreach (XmlNode xmlEntry in xmlEntries)
        {
          DictionaryEntry dictEntry = new DictionaryEntry();
          dictEntry.GenericType = xmlEntry["GenericType"].InnerText;

          switch (xmlEntry["StrictType"].InnerText.ToLower())
          {
            case "binary": dictEntry.StrictType = StrictDataType.Binary; break;
            case "varbinary": dictEntry.StrictType = StrictDataType.VarBinary; break;
            case "bool": dictEntry.StrictType = StrictDataType.Bool; break;
            case "char": dictEntry.StrictType = StrictDataType.Char; break;
            case "datetime": dictEntry.StrictType = StrictDataType.DateTime; break;
            case "time": dictEntry.StrictType = StrictDataType.Time; break;
            case "decimal": dictEntry.StrictType = StrictDataType.Decimal; break;
            case "float": dictEntry.StrictType = StrictDataType.Float; break;
            case "int": dictEntry.StrictType = StrictDataType.Int; break;
            case "unsigneddecimal": dictEntry.StrictType = StrictDataType.UnsignedDecimal; break;
            case "unsignedfloat": dictEntry.StrictType = StrictDataType.UnsignedFloat; break;
            case "unsignedint": dictEntry.StrictType = StrictDataType.UnsignedInt; break;
            case "varchar": dictEntry.StrictType = StrictDataType.VarChar; break;
            default: throw new Exception("Unsupported strictDateType in Convert Option: " + xmlEntry["StrictType"].InnerText);
          }

          dictEntry.StrictTypeIsNullable = null;
          foreach (XmlAttribute attribute in xmlEntry["StrictType"].Attributes)
          {
            if (attribute.Name == "isNullable")
            {
              dictEntry.StrictTypeIsNullable = Boolean.Parse(attribute.Value);
            }
          }

          String[] rawSizes = xmlEntry["GenericSize"].InnerText.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          dictEntry.GenericSize = new Int64[rawSizes.Length];
          for (int i = 0; i < rawSizes.Length; i++)
          {
            dictEntry.GenericSize[i] = Int64.Parse(rawSizes[i]);
          }

          rawSizes = xmlEntry["StrictSize"].InnerText.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
          dictEntry.StrictSize = new Int64[rawSizes.Length];
          for (int i = 0; i < rawSizes.Length; i++)
          {
            dictEntry.StrictSize[i] = Int64.Parse(rawSizes[i]);
          }
          this.entries.Add(dictEntry);
        }

        xmlEntries = xmlDoc.GetElementsByTagName("Option");
        options.Clear();
        convertOptions.Clear();
        foreach (XmlNode xmlEntry in xmlEntries)
        {
          String optionType = xmlEntry["Type"].InnerText;

          if (optionType.ToUpper() == "CONVERT")
          {
            String[] rawSizes = xmlEntry["Arguments"]["GenericSize"].InnerText.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Int64[] genericSizes = new Int64[rawSizes.Length];
            for (int i = 0; i < rawSizes.Length; i++)
            {
              genericSizes[i] = Int64.Parse(rawSizes[i]);
            }

            rawSizes = xmlEntry["Arguments"]["StrictSize"].InnerText.Split(new Char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            Int64[] strictSizes = new Int64[rawSizes.Length];
            for (int i = 0; i < rawSizes.Length; i++)
            {
              strictSizes[i] = Int64.Parse(rawSizes[i]);
            }
            ConvertOption co = new ConvertOption(xmlEntry["Arguments"]["GenericType"].InnerText,
                                                 genericSizes,
                                                 xmlEntry["Arguments"]["StrictType"].InnerText,
                                                 strictSizes);
            this.convertOptions.Add(co);
          }
          else
          {
            DictionaryOption option = new DictionaryOption();
            option.Type = optionType;

            foreach (XmlNode arg in xmlEntry["Arguments"].ChildNodes)
            {
              option.Args.Add(arg.Name, arg.InnerText);
            }
            this.options.Add(option);
          }
        }
      }
      catch (XmlException e)
      {
        return false;
      }
      return true;
    }

  }
}
