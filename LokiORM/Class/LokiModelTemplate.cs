﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace LokiORM
{
  class Pool
  {
    private String name;
    private String type;
    private String content;
    private String separator;

    public String Name
    { get { return this.name; } }
    public String Type
    { get { return this.type; } }
    public String Content
    { get { return this.content; } }
    public String Separator
    { get { return this.separator; } }

    public Pool(String name, String type, String content, String separator)
    {
      this.name = name;
      this.type = type;
      this.content = content;
      this.separator = separator;
    }
  }

  class Assign
  {
    private String dataType;
    private String content;

    public String DataType
    { get { return this.dataType; } }
    public String Content
    { get { return this.content; } }

    public Assign(String dataType, String content)
    {
      this.dataType = dataType;
      this.content = content;
    }
  }

  class Field
  {
    private String name;
    private String value;

    public Field(String name, String value)
    {
      this.name = name;
      this.value = value;
    }

    public String Value
    {
      get { return this.value; }
      set { this.value = value; }
    }


    public String Name
    {
      get { return this.name; }
      set { this.name = value; }
    }


  }

  class LokiModelTemplate
  {
    private XmlDocument xmlDoc;
    //private UnifiedXml handle;
    private String name;
    private String dictionaryName;
    private String fileName;
    private LokiDictionary dictionary;
    private List<Pool> pools = new List<Pool>();
    private Dictionary<String, Assign> assigns = new Dictionary<String, Assign>();
    private Dictionary<String, String> schemes = new Dictionary<String, String>();
    private List<Field> fields = new List<Field>();
    private List<string> schemesOrder = new List<string>();

    private static List<LokiModelTemplate> list = new List<LokiModelTemplate>();

    public String Name
    {
      get
      {
        return this.name;
      }
    }

    public String DictionaryName
    {
      get
      {
        return this.dictionaryName;
      }
    }

    public String FileName
    { get { return this.fileName; } }

    public LokiDictionary Dictionary
    {
      get
      {
        return this.dictionary;
      }

      set
      {
        this.dictionary = value;
      }
    }

    public Dictionary<String, String> Schemes
    {
      get { return this.schemes; }
    }

    public List<Field> Fields
    {
      get { return this.fields; }
    }

    public static List<LokiModelTemplate> List
    {
      get
      {
        return list;
      }
    }

    public Boolean AcquireFromXml(String filepath)
    {
      this.xmlDoc = new XmlDocument();

      using (FileStream reader = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.Read))
      {
        this.xmlDoc.Load(reader);
      }

      //this.handle = new UnifiedXml(filepath);
      //      if (!this.handle.Read())
      //      {
      //          return false;
      //      }

      try
      {
        XmlDocument xmlDoc = this.xmlDoc;// handle.XmlDoc;

        this.name = xmlDoc.GetElementsByTagName("Info")[0]["Name"].InnerText;
        this.dictionaryName = xmlDoc.GetElementsByTagName("Info")[0]["Dictionary"].InnerText;
        this.fileName = xmlDoc.GetElementsByTagName("Info")[0]["FileName"].InnerText;

        XmlNodeList xmlList = xmlDoc.GetElementsByTagName("Field");
        fields.Clear();
        foreach (XmlNode xmlField in xmlList)
        {
          String key = xmlField["Name"].InnerText;
          fields.Add(new Field(key, ""));
        }

        xmlList = xmlDoc.GetElementsByTagName("SchemesOrder");
        schemesOrder.Clear();
        foreach (XmlNode xmlOrderContainer in xmlList)
        {
          foreach (XmlNode xmlOrder in xmlOrderContainer.ChildNodes)
          {
            schemesOrder.Add(xmlOrder.InnerText);
          }
        }

        xmlList = xmlDoc.GetElementsByTagName("Scheme");
        schemes.Clear();
        foreach (XmlNode xmlScheme in xmlList)
        {
          String key = xmlScheme["Type"].InnerText;
          String value = xmlScheme["Content"].InnerText;// UnifiedXml.UnhashString(xmlScheme["Content"].InnerText);
          schemes.Add(key, value);
        }

        xmlList = xmlDoc.GetElementsByTagName("Assign");
        assigns.Clear();
        foreach (XmlNode xmlAssign in xmlList)
        {
          //Assign assign = new Assign(UnifiedXml.UnhashString(xmlAssign["DataType"].InnerText), UnifiedXml.UnhashString(xmlAssign["Content"].InnerText));
          Assign assign = new Assign(xmlAssign["DataType"].InnerText, xmlAssign["Content"].InnerText);
          assigns.Add(assign.DataType, assign);
        }

        xmlList = xmlDoc.GetElementsByTagName("Pool");
        pools.Clear();
        foreach (XmlNode xmlPool in xmlList)
        {
          //Pool pool = new Pool(xmlPool["Name"].InnerText, xmlPool["Type"].InnerText.ToUpper(), UnifiedXml.UnhashString(xmlPool["Content"].InnerText), UnifiedXml.UnhashString(xmlPool["Separator"].InnerText));
          Pool pool = new Pool(xmlPool["Name"].InnerText, xmlPool["Type"].InnerText.ToUpper(), xmlPool["Content"].InnerText, xmlPool["Separator"].InnerText);
          pools.Add(pool);
        }
      }
      catch (XmlException e)
      {
        return false;
      }
      return true;
    }

    public Boolean FindDictionary()
    {
      foreach (LokiDictionary dict in LokiDictionary.List)
      {
        if (dict.DataPattern == this.dictionaryName)
        {
          this.dictionary = dict;
          return true;
        }
      }
      return false;
    }

    public override string ToString()
    {
      return this.name;
    }

    public string[] SchemesOrder
    {
      get
      {
        return this.schemesOrder.ToArray();
      }
    }

    private void InitialParse(ClassInfo ci)
    {
      ci.Name = ci.Name.Replace(":", "");
      foreach (VariableInfo vi in ci.Vars.Values)
      {
        vi.Name = vi.Name.Replace(":", "");
      }
    }

    public string ParseString(string stringToParse, ClassInfo ci, Dictionary<String, String> options)
    {
      this.InitialParse(ci);
      string data = stringToParse.Replace("[[tableName]]", ci.Name);
      data = data.Replace("[[className]]", Char.ToLower(ci.Name[0]) + ci.Name.Substring(1));
      data = data.Replace("[[ClassName]]", Char.ToUpper(ci.Name[0]) + ci.Name.Substring(1));
      if (ci.ParentClass != null)
      {
        data = data.Replace("[[parentClassName]]", Char.ToLower(ci.ParentClass.Name[0]) + ci.ParentClass.Name.Substring(1));
        data = data.Replace("[[ParentClassName]]", Char.ToUpper(ci.ParentClass.Name[0]) + ci.ParentClass.Name.Substring(1));
      }
      else
      {
        data = data.Replace("[[parentClassName]]", options["parentClassName"]);
        data = data.Replace("[[ParentClassName]]", Char.ToUpper(options["parentClassName"][0]) + options["parentClassName"].Substring(1));
      }

      if (data.Contains("[[firstPrimaryKey]]"))
      {
        foreach (VariableInfo vi in ci.Vars.Values)
        {
          if (vi.Identifying)
          {
            data = data.Replace("[[firstPrimaryKey]]", Char.ToLower(vi.Name[0]) + vi.Name.Substring(1));
            break;
          }
        }
      }

      if (data.Contains("[[FirstPrimaryKey]]"))
      {
        foreach (VariableInfo vi in ci.Vars.Values)
        {
          if (vi.Identifying)
          {
            data = data.Replace("[[FirstPrimaryKey]]", Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1));
            break;
          }
        }
      }

      foreach (Field field in this.fields)
      {
        if (data.Contains("[[Field" + field.Name + "]]"))
        {
          data = data.Replace("[[Field" + field.Name + "]]", field.Value);// UnifiedXml.HashString(field.Value));
        }
      }

      foreach (Pool pool in this.pools)
      {
        if (data.Contains("[[Pool" + pool.Name + "]]"))
        {
          String text = "";
          foreach (VariableInfo vi in ci.Vars.Values)
          {
            switch (pool.Type)
            {
              case "VARIABLE":
                text += this.ParseVariable(pool.Content, vi, options) + pool.Separator;
                break;
              case "PRIMARY":
                if (vi.Identifying) text += this.ParseVariable(pool.Content, vi, options) + pool.Separator;
                break;
              case "NONPRIMARY":
                if (!vi.Identifying) text += this.ParseVariable(pool.Content, vi, options) + pool.Separator;
                break;
            }
          }
          if (text.Length > 0)
          {
            text = text.Substring(0, text.Length - pool.Separator.Length);
          }

          data = data.Replace("[[Pool" + pool.Name + "]]", text);
        }
      }
      data = data.Replace("[[NL]]", Environment.NewLine);
      return data;
    }

    public String Parse(String template, ClassInfo ci, Dictionary<String, String> options)
    {
      this.InitialParse(ci);
      String data = this.schemes[template];

      return this.ParseString(data, ci, options);
    }

    public String Parse(String template, VariableInfo vi, Dictionary<String, String> options)
    {
      String data = this.schemes[template];

      return this.ParseVariable(data, vi, options);
    }

    public String ParseVariable(String text, VariableInfo vi, Dictionary<String, String> options)
    {
      text = text.Replace("[[access]]", options["access"]);
      text = text.Replace("[[dataType]]", vi.GenericStructure.DataType);
      text = text.Replace("[[dataSize]]", vi.DataSize);
      text = text.Replace("[[defaultValue]]", (vi.DefaultValue == null || vi.DefaultValue is DBNull) ? "null" : vi.DefaultValue.ToString());
      text = text.Replace("[[orginalName]]", vi.OrginalName);
      text = text.Replace("[[variableName]]", Char.ToLower(vi.Name[0]) + vi.Name.Substring(1));
      text = text.Replace("[[VariableName]]", Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1));
      text = text.Replace("[[NL]]", Environment.NewLine);

      if (text.Contains("[[assign]]"))
      {
        foreach (Assign assign in assigns.Values)
        {
          if (vi.GenericStructure.DataType == assign.DataType || assign.DataType == "__ANY__")
          {
            text = text.Replace("[[assign]]", this.ParseVariable(assign.Content, vi, options));
            break;
          }
        }
      }

      return text;
    }


    public void GenerateModelForCSharp(ClassInfo ci, StreamWriter writer)
    {
      String className = Char.ToUpper(ci.Name[0]) + ci.Name.Substring(1).Replace(":", "");
      String text = "";

      writer.Write("using System;" + Environment.NewLine + "using System.Collections.Generic;" + Environment.NewLine +
      "using System.Data;" + Environment.NewLine + "using System.Linq;" + Environment.NewLine +
      "using System.Text;" + Environment.NewLine + Environment.NewLine);

      writer.Write("namespace [name] " + Environment.NewLine + "{" + Environment.NewLine + "\tclass " + className + " : " + (ci.ParentClass == null ? "LokiModel" : ci.ParentClass.Name) + Environment.NewLine +
                   "\t{" + Environment.NewLine + "\t\t#region accessors & attributes" + Environment.NewLine);

      foreach (VariableInfo vi in ci.Vars.Values)
      {
        String type = vi.GenericStructure.DataType;

        String lname = Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");
        String uname = Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");

        writer.Write("\t\tprotected " + type + " " + lname);

        if (vi.DefaultValue.ToString().Length > 0)
        {
          switch (type)
          {
            case "Boolean":
              writer.Write(" = " + vi.DefaultValue.ToString().ToLower());
              break;
            default:
              writer.Write(" = " + vi.DefaultValue.ToString());
              break;

          }
        }

        writer.WriteLine(";");
        writer.WriteLine("\t\tpublic " + type + " " + uname + Environment.NewLine + "\t\t{" +
                     Environment.NewLine + "\t\t\tget { return this." + lname + "; }");
        writer.WriteLine("\t\t\tset" + Environment.NewLine + "\t\t\t{");
        writer.WriteLine("\t\t\t\tif(value != this." + lname + " && Coherency == ModelCoherency.Coherent)" + Environment.NewLine + "\t\t\t\t{");
        writer.WriteLine("\t\t\t\t\tCoherency = ModelCoherency.Incoherent;" + Environment.NewLine + "\t\t\t\t}");
        writer.WriteLine("\t\t\t\tthis." + lname + " = value;" + Environment.NewLine + "\t\t\t}" + Environment.NewLine + "\t\t}");
      }

      writer.Write("\t\t#endregion" + Environment.NewLine + Environment.NewLine);

      writer.WriteLine("\t\t#region Data manipulation");

      // DataFetch
      writer.WriteLine("\t\tprotected override Boolean DataFetch()");
      writer.WriteLine("\t\t{");

      writer.Write("\t\t\tDataTable dt = connector.Select(\"" + ci.Name + "\", new List<String>(){}, new Dictionary<String, Object>(){");

      text = "";
      foreach (VariableInfo vi in ci.Vars.Values)
      {
        if (!vi.Identifying)
        { continue; }
        String lname = Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");

        text += "{\"" + vi.Name + "\", " + lname + "}, ";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 2);
      }
      writer.Write(text);

      writer.WriteLine("});" + Environment.NewLine);
      writer.WriteLine("\t\t\tif(dt.Rows.Count == 0)");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\treturn false;");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\telse");
      writer.WriteLine("\t\t\t{");

      foreach (VariableInfo vi in ci.Vars.Values)
      {
        if (!vi.Identifying)
        {
          writer.Write("\t\t\t\tthis." + Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "") + " = ");
          switch (vi.GenericStructure.DataType)
          {
            case "String":
              writer.WriteLine("dt.Rows[0][\"" + vi.OrginalName + "\"].ToString();");
              break;
            case "Boolean":
              writer.WriteLine("Connector.BitToBoolean(dt.Rows[0][\"" + vi.OrginalName + "\"]);");
              break;
            case "Datetime":
              writer.WriteLine("DateTime.Parse(dt.Rows[0][\"" + vi.OrginalName + "\"]);");
              break;
            default:
              writer.WriteLine(vi.GenericStructure.DataType + ".Parse(dt.Rows[0][\"" + vi.OrginalName + "\"].ToString());");
              break;

          }
        }
      }
      writer.WriteLine("\t\t\t\treturn true;");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // DataInsert
      writer.WriteLine("\t\tprotected override void DataInsert()");
      writer.WriteLine("\t\t{");
      writer.Write("\t\t\t" + Char.ToUpper(ci.IdentifyingVars[0].Name[0]) + ci.IdentifyingVars[0].Name.Substring(1).Replace(":", "") + " = (" + ci.IdentifyingVars[0].GenericStructure.DataType + ")connector.Insert(\"" + ci.Name + "\", new Dictionary<String, Object>() {");

      text = "";
      foreach (VariableInfo vi in ci.Vars.Values)
      {
        String uname = Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");
        text += "{\"" + vi.OrginalName + "\", " + uname + "},";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 1);
      }
      writer.Write(text);

      writer.WriteLine("});");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // DataUpdate
      writer.WriteLine("\t\tprotected override Int32 DataUpdate()");
      writer.WriteLine("\t\t{");
      writer.Write("\t\t\treturn connector.Update(\"" + ci.Name + "\", new Dictionary<String, Object>() {");

      text = "";
      foreach (VariableInfo vi in ci.Vars.Values)
      {
        if (vi.Identifying) { continue; }
        String uname = Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");
        text += "{\"" + vi.OrginalName + "\", " + uname + "}" + ", ";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 2);
      }
      writer.Write(text);

      writer.Write("}, new Dictionary<String, Object>() {");

      text = "";
      foreach (VariableInfo vi in ci.IdentifyingVars)
      {
        String uname = Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");
        text += "{\"" + vi.OrginalName + "\", " + uname + "}" + ", ";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 2);
      }
      writer.Write(text);

      writer.WriteLine("});");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // DataDelete
      writer.WriteLine("\t\tprotected override Int32 DataDelete()");
      writer.WriteLine("\t\t{");
      writer.Write("\t\t\treturn connector.Delete(\"" + ci.Name + "\", new Dictionary<String, Object>() {");

      text = "";
      foreach (VariableInfo vi in ci.IdentifyingVars)
      {
        String uname = Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");
        text += "{\"" + vi.OrginalName + "\", " + uname + "}" + ", ";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 2);
      }
      writer.Write(text);

      writer.WriteLine("});");
      writer.WriteLine("\t\t}");

      writer.Write("\t\t#endregion" + Environment.NewLine + Environment.NewLine);

      writer.Write("\t\tprivate " + className + "(){}" + Environment.NewLine + Environment.NewLine);

      // Acquire
      writer.Write("\t\tpublic static " + className + " Acquire(");

      text = "";
      foreach (VariableInfo vi in ci.Vars.Values)
      {

        if (!vi.Identifying)
        { continue; }
        String lname = Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");

        text += vi.GenericStructure.DataType + " " + lname + ", ";
      }
      if (text.Length > 0)
      {
        text = text.Substring(0, text.Length - 2);
      }
      writer.Write(text);

      writer.WriteLine(")");
      writer.WriteLine("\t\t{");
      writer.WriteLine("\t\t\t" + className + " aux = new " + className + "();");
      foreach (VariableInfo vi in ci.IdentifyingVars)
      {
        String lname = Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "");

        writer.WriteLine("\t\t\taux." + Char.ToUpper(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "") + " = " + Char.ToLower(vi.Name[0]) + vi.Name.Substring(1).Replace(":", "") + ";");
      }
      writer.WriteLine("\t\t\tif(aux.SynchronizeWeak())");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\treturn aux;");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\telse");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\treturn null;");
      writer.WriteLine("\t\t\t}" + Environment.NewLine + "\t\t}" + Environment.NewLine);
      // Synchronize
      writer.WriteLine("\t\tpublic override void Synchronize()");
      writer.WriteLine("\t\t{");
      writer.WriteLine("\t\t\tif (Coherency == ModelCoherency.Coherent)");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\tthis.SynchronizeWeak();");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\telse");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\tthis.SynchronizeStrong();");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // SynchronizeStrong
      writer.WriteLine("\t\tpublic override Boolean SynchronizeStrong()");
      writer.WriteLine("\t\t{");
      writer.WriteLine("\t\t\tif(this.Coherency == ModelCoherency.NonExistent)");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\tthis.DataInsert();");
      if (ci.ParentClass != null)
      { writer.WriteLine("\t\t\t\tbase.SynchronizeStrong();"); }
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\telse");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\tthis.DataUpdate();");
      if (ci.ParentClass != null)
      { writer.WriteLine("\t\t\t\tbase.SynchronizeStrong();"); }
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\tthis.Coherency = ModelCoherency.Coherent;");
      writer.WriteLine("\t\t\treturn true;");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // SynchronizeWeak
      writer.WriteLine("\t\tpublic override Boolean SynchronizeWeak()");
      writer.WriteLine("\t\t{");
      writer.WriteLine("\t\t\tif(this.DataFetch())");
      writer.WriteLine("\t\t\t{");
      if (ci.ParentClass != null)
      { writer.WriteLine("\t\t\t\tbase.SynchronizeWeak();"); }
      writer.WriteLine("\t\t\t\tthis.Coherency = ModelCoherency.Coherent;");
      writer.WriteLine("\t\t\t\treturn true;");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t\telse");
      writer.WriteLine("\t\t\t{");
      writer.WriteLine("\t\t\t\tthis.Coherency = ModelCoherency.NonExistent;");
      writer.WriteLine("\t\t\t\treturn false;");
      writer.WriteLine("\t\t\t}");
      writer.WriteLine("\t\t}" + Environment.NewLine);
      // Remove
      writer.WriteLine("\t\tpublic override void Remove()");
      writer.WriteLine("\t\t{");
      writer.WriteLine("\t\t\tthis.DataDelete();");
      if (ci.ParentClass != null)
      { writer.WriteLine("\t\t\tbase.Remove();"); }
      writer.WriteLine("\t\t\tthis.Coherency = ModelCoherency.NonExistent;");
      writer.WriteLine("\t\t}");

      writer.Write("\t} " + Environment.NewLine + "}");
    }
  }
}
