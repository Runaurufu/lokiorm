﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Runaurufu.Data.Connection;

namespace LokiORM
{
  class Converter
  {
    private IDatabaseConnectorNativeConnection connector = null;
    private String exportPath;
    private LokiModelTemplate template = new LokiModelTemplate();
    private LokiDictionary importDictionary = null;
    private LokiDictionary exportDictionary = null;
    private List<ConvertOption> enabledImportConversions = new List<ConvertOption>();
    private List<ConvertOption> enabledExportConversions = new List<ConvertOption>();
    private List<ClassInfo> classes = new List<ClassInfo>();

    public IDatabaseConnectorNativeConnection Connector
    {
      get { return this.connector; }
      set { this.connector = value; }
    }

    public LokiModelTemplate Template
    {
      get { return this.template; }
      set { this.template = value; }
    }

    public LokiDictionary ImportDictionary
    {
      get { return this.importDictionary; }
      set { this.importDictionary = value; }
    }

    public LokiDictionary ExportDictionary
    {
      get { return this.exportDictionary; }
      set { this.exportDictionary = value; }
    }

    public List<ClassInfo> Classes
    {
      get { return this.classes; }
    }

    public List<ConvertOption> EnabledImportConversions
    {
      get { return this.enabledImportConversions; }
    }

    public List<ConvertOption> EnabledExportConversions
    {
      get { return this.enabledExportConversions; }
    }

    public String ExportPath
    {
      get { return this.exportPath; }
      set { this.exportPath = value; }
    }

    public void PerformImportConversion()
    {
      foreach (ClassInfo item in this.classes)
      {
        item.GenerateStrictType(this.importDictionary, this.enabledImportConversions);
      }
    }

    public void PerformExportConversion()
    {
      foreach (ClassInfo item in this.classes)
      {
        item.GenerateGenericType(this.exportDictionary, this.enabledExportConversions);
      }
    }

    public void GetProperDatabaseImportDictionary(List<LokiDictionary> list)
    {
      string connectorType = connector.GetType().FullName;

      foreach (LokiDictionary dict in list)
      {
        if (string.Equals(dict.DataPattern, connectorType, StringComparison.InvariantCultureIgnoreCase))
        {
          this.importDictionary = dict;
          break;
        }
      }
    }

    public void GetDatabaseStructure()
    {
      Dictionary<String, TableStructure> stru = this.connector.GetDatabaseStructure();
      this.classes.Clear();
      foreach (TableStructure ts in stru.Values)
      {
        ClassInfo ci = new ClassInfo(ts);
        this.classes.Add(ci);
      }
    }

  }
}
